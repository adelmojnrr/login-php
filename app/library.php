<?php

class DemoLib 
{
  public function Register($nome, $data_de_nascimento, $sexo, $email, $senha) {
    try {
      $db = DB();
      $query = $db->prepare("INSERT INTO users(nome,  data_de_nascimento, sexo, email, senha) VALUES(:nome,:data_de_nascimento,:sexo,:email,:senha);");
      $query->bindParam(":nome", $nome, PDO::PARAM_STR);
      $query->bindParam(":data_de_nascimento", $data_de_nascimento, PDO::PARAM_INT);
      $query->bindParam(":sexo", $sexo, PDO::PARAM_STR);
      $query->bindParam(":email", $email, PDO::PARAM_STR);
      $enc_senha = hash(":senha", $password);
      $query->bindParam(":senha", $enc_senha, PDO::PARAM_STR); 
      $query->execute();
      return $db->lastInsertId();
    } catch(PDOException $e) {
      exit($e->getMessage());
    }
  }

  public function isEmail() {
    try{
      $db = DB();
      $query = $db->prepare("SELECT id FROM users"  . " WHERE email=:email");
      $query->bindParam("email", $email, PDO::PARAM_STR);
      $query->execute();
      if($query->rowCount() > 0) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $e) {
      exit($e->getMessage());
    }
  }
}

