<?php
  session_start();

  require './library.php';

  $app = new DemoLib();
  
  if(!empty($_POST['btnRegister'])){
    if($_POST['nome']){
      $register_error_message = 'É preciso digitar seu nome';
    } else if($_POST['email']){
      $register_error_message = 'É preciso digitar seu email';
    } else if($_POST['senha']){
      $register_error_message = 'É preciso digitar sua senhar';
    } else if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
      $register_error_message = 'Endereço do email está inválido';
    } else if($app->isEmail($_POST['email'])){
      $register_error_message = 'Esse endereço de email já está em uso';
    } else{
      $id = $app->Register($_POST['nome'], $_POST['dataDeNascimento'], $_POST['sexo'], $_POST['email'], $_POST['password']);
      $_SESSION['id'] = $id;
      header("Location: profile.php");
    }
}
?>

