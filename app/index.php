<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="./assets/css/normalize.css">

    <link rel="stylesheet" href="./assets/css/main.css">
 <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900" rel="stylesheet"> 
    <title>3DMoove</title>
</head>
<body>
    <header class="header">
        <div class="header-brand">
            <img src="./assets/images/logo_header_white.png" alt="" class="header-brand-img">
        </div>
        <form class="hidden-mobile" action="">
            <input class="input-email" placeholder="Email" type="email" name="email" required="required">
            <input class="input-email" placeholder="Senha" type="password" name="senha" required="required">
            <button class="btn btn-entrar">ENTRAR</button>
             <a href="#" class="link-esqueceu-senha">Esqueceu sua senha?</a>

        </form>
    </header>
</div>
<div class="container">
    <div class="container-left">
        <h2 class="h2">CHEGAMOS PARA DESAFIAR <span class="span-vc">VOCÊ</span></h2>
        <form class="form-mobile hidden-pc" action="">
            <input class="input-email" placeholder="Email" type="email" name="email">
            <input class="input-password"  placeholder="Senha" type="password" name="senha">
            <button type="submit" class="btn">Entrar</button>
        </form>
        <button class="btn hidden-pc btn-cadastrar">Cadastrar-se</button>

        <div class="text">
            <p class="p">TREINAMENTO FUNCIONAL</p>
            <p>Aulas em time com metodologia própria, livre de mensalidades ou pacotes</p>
            <button type="button" class="btn">SAIBA MAIS</button>
        </div>
    </div>
<div class="container-right">
        <h2 class="h2 hidden-mobile">ABRA SUA CONTA <br><span class="span">E GANHE MOOVE COINS</span></h2>
    <form action="register.php" method="POST" class="hidden-mobile">
        <div class="input-group">
            <label for="nome">Nome(completo)</label>
            <input class="input-form-cadastro" placeholder="Nome" id="nome" name="nome" type="text" required="required">
        </div>
        <div class="input-group">
            <label for="dataDeNascimento">Data de nascimento</label>
            <input class="input-form-cadastro" id="dataDeNascimento" maxlength="10" name="dataDeNascimento" onkeyup="data(this)" type="text" pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" placeholder="Data de nascimento" required="required">
        </div>
        <div class="input-group">
            <label for="sexo">Sexo</label>
            <select class="select-sexo" name="sexo" id="sexo" required="required">
                <option name="masculino">Masculino</option>
                <option name="feminino">Feminino</option>
            </select>
        </div>
        <div class="input-group">
            <label for="email">Email</label>
            <input class="input-form-cadastro" placeholder="Email" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" onkeyup="emailConfirmation()" type="email" required="required">
        </div>

        <div class="input-group" id="emailConfirmation">
             <label for="email">Confirme o email</label>
            <input class="input-form-cadastro" placeholder="Email"  name="email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required="required">
        </div>
        <div class="input-group">
            <label for="password">Senha</label>
            <input class="input-form-cadastro" placeholder="Senha" id="password" name="password" type="password" required="required">
        </div>
        <button type="submit" name="btnRegister" class="btn">CADASTRAR</button>
    </form>
</div>
</div>
</body>
<script src="./assets/js/data.js"></script>
</html>
