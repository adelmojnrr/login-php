var input1 = document.querySelector("#email");
var inputConfirmation = document.querySelector("#emailConfirmation");

function emailConfirmation(){
  if(input1.value.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)){
	return inputConfirmation.style.display = "block";
}
}
  
function data(date){
     var v = date.value;
        if (v.match(/^\d{2}$/) !== null) {
            date.value = v + '/';
        } else if (v.match(/^\d{2}\/\d{2}$/) !== null) {
            date.value = v + '/';
        }
  return v;
}
